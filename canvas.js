const canvas = document.querySelector("canvas");
const Form = document.getElementById("myform");
const submit = document.getElementById("myform-submit");
canvas.width = 800;
canvas.height = 800;
var ctx = canvas.getContext("2d");

var RADIUS = 5;
// var PHI = (Math.sqrt(5) - 1) / 2;
var PHI = (Math.sqrt(5) + 1) / 2 - 1;

function dot(ctx, x, y) {
  ctx.beginPath();
  ctx.arc(x, y, RADIUS, 0, 2 * Math.PI, false);
  ctx.fillStyle = "blue";
  ctx.fill();
}

function redDot(ctx, x, y) {
  ctx.beginPath();
  ctx.arc(x, y, 0.5 * RADIUS, 0, 2 * Math.PI, false);
  ctx.fillStyle = "red";
  ctx.fill();
}

function polarToCartesian(r, theta) {
  return {
    x: r * Math.cos(theta),
    y: r * Math.sin(theta),
  };
}

function _distance(point1, point2) {
  let a = point1.x - point2.x;
  let b = point1.y - point2.y;
  return Math.sqrt(a * a + b * b);
}

function lerp(point1, point2, lerp) {
  let x = point1.x + (point2.x - point1.x) * lerp;
  let y = point1.y + (point2.y - point1.y) * lerp;

  return { x, y };
}

submit.addEventListener("click", (e) => {
  e.preventDefault();
  const node = myform.node.value;
  const wave = myform.wave.value;
  const height = myform.height.value;
  const diameter = myform.diameter.value;
  if (node == "" || wave == "" || height == "" || diameter == "") {
    alert("Please provide your all the inputs!");
  } else {
    let samples = parseInt(node);
    let numWaves = parseInt(wave);
    let waveHeight = parseInt(height);
    let circleDiameter = parseInt(diameter);

    console.log(samples, numWaves, waveHeight, circleDiameter);

    ctx.clearRect(0, 0, 800, 800);

    // each node getting put at 1 degree, spacing is off.
    // var numWaves = 10
    // var circleDiameter = 200
    // var waveHeight = 100
    // const DEG2RAD = (Math.PI / 180)
    // for (var n = 0; n < number; n++) {
    //   var theta = n * DEG2RAD
    //   var r = circleDiameter + (waveHeight * Math.sin(numWaves * theta))
    //   var pt = polarToCartesian(r, theta)
    //   dot(ctx, pt.x + 400, pt.y + 400)
    // }

    // Step one generate point on the shape:
    // let samples = 1000;
    // let numWaves = 10;
    // let circleDiameter = 200;
    // let waveHeight = 100;
    const DEG2RAD = Math.PI / 180;
    let points = [];

    for (let n = 0; n < samples; n++) {
      let theta = n * DEG2RAD * (360 / samples);
      let r = circleDiameter + waveHeight * Math.sin(numWaves * theta);
      let pt = polarToCartesian(r, theta);
      points.push({ x: pt.x + 400, y: pt.y + 400 });

      redDot(ctx, points[n].x, points[n].y);
    }

    // step two. calculate total distance between points
    let totalDistance = 0;
    for (let n = 0; n < points.length - 1; n++) {
      let point = points[n];
      let nextPoint = points[n + 1];
      let distance = _distance(point, nextPoint);
      point.distance = distance;
      totalDistance += distance;
    }
    // map the last point back to the first (making a lazy assumtion)
    points[points.length - 1].distance = points[0].distance;
    totalDistance += points[0].distance;

    // pick a desired amount of evenly spaced points and walk around shape to calculate them
    let desiredPoints = 200;
    let distanceBetweenPoints = totalDistance / desiredPoints;
    let j = 0;
    let evenPoints = [];
    let carryOverDistance = 0;
    for (let i = 0; i < desiredPoints; i++) {
      let accumulatedDistance = carryOverDistance;
      let nextDistance = points[j].distance;
      let lastPoint = points[j];
      let wrapped = false;
      while (accumulatedDistance + nextDistance < distanceBetweenPoints) {
        accumulatedDistance += nextDistance;
        lastPoint = points[j];
        j++;
        if (j === points.length) {
          j = 0;
          wrapped = true;
        }
        nextDistance = points[j].distance;
      }
      if (j === 0 && wrapped === false) {
        j++;
      }
      if (j === points.length) {
        j = 0;
      }
      let distanceTraveledLine = distanceBetweenPoints - accumulatedDistance;
      let remainingTravelPercent = distanceTraveledLine / nextDistance;
      let finalPoint = lerp(lastPoint, points[j], remainingTravelPercent);

      evenPoints.push(finalPoint);

      // dot(ctx, finalPoint.x, finalPoint.y);

      carryOverDistance = nextDistance - distanceTraveledLine;
      j++;
    }
  }
});
